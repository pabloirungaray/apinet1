﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Api.Models;
namespace Api.Controllers
{
    public class PersonaController : ApiController
    {
        // GET: api/Persona
        public IHttpActionResult Get()
        {
            midbEntities midb = new midbEntities();
            List<Persona> lista = midb.Personas.ToList();
            return Ok(lista);
        }

        // GET: api/Persona/5

        public IHttpActionResult Get(int id)
        {
            midbEntities midb = new midbEntities(); // Clase se encuentra en modelo  Context tt
            Persona persona = midb.Personas.Find(id);// Clase se encuentra en modelo tt
            return Ok(persona);
        }

        // POST: api/Persona
        public IHttpActionResult Post([FromBody]Persona perona_insertada)
        {
            //midbEntities midb = new midbEntities();
            midbEntities midb = new midbEntities();
            midb.Personas.Add(perona_insertada);
            midb.SaveChanges();
            return Ok();
                        
            
        }

        // PUT: api/Persona/5
        public IHttpActionResult Put(int id, [FromBody]Persona personaM)
        {
            midbEntities midb = new midbEntities();
            Persona persona = new Persona();
            if(persona == null)
            {
                return NotFound();
            }
            persona.Nombre = personaM.Nombre;
            persona.Apellido = personaM.Apellido;
            persona.Telefono = personaM.Telefono;
            midb.SaveChanges();
            return Ok();
        }

        // DELETE: api/Persona/5
        public IHttpActionResult Delete(int id)
        {
            midbEntities midb = new midbEntities();
            Persona persona = midb.Personas.Find(id);
            if (persona == null)
            {
                return NotFound();
            }
            midb.Personas.Remove(persona);
            midb.SaveChanges();
            return Ok();
        }
    }
}
