﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Api.Models;
namespace Api.Controllers
{
    public class ProductoController : ApiController
    {
        // GET: api/Producto
        public IHttpActionResult Get()
        {
            midbEntities midb = new midbEntities();
            List<Producto> lista = midb.Productoes.ToList();
            return Ok(lista);
        }

        // GET: api/Producto/5
        public IHttpActionResult Get(int id)
        {
            midbEntities midb = new midbEntities();
            Producto producto = midb.Productoes.Find(id);
            return Ok(producto);
        }

        // POST: api/Producto
        public IHttpActionResult Post([FromBody] Producto productoAdd)
        {
            midbEntities midb = new midbEntities();
            midb.Productoes.Add(productoAdd);
            midb.SaveChanges();
            return Ok();
        }

        // PUT: api/Producto/5
        public IHttpActionResult Put(int id, [FromBody]Producto productoM)
        {
            midbEntities midb = new midbEntities();
            Producto producto = new Producto();
            if (producto == null)
            {
                return NotFound();
            }
            producto.nombre = productoM.nombre;
            producto.precio = productoM.precio;
            producto.imagen = productoM.imagen;
            midb.SaveChanges();
            
            return Ok("aaa");
        }

        // DELETE: api/Producto/5
        public IHttpActionResult Delete(int id)
        {
            midbEntities midb = new midbEntities();
            Producto producto = midb.Productoes.Find(id);
            if (producto == null)
            {
                return NotFound();
            }
            midb.Productoes.Remove(producto);
            midb.SaveChanges();
            return Ok();
        }
    }
}
